var dbconfig = require('../config/conf');
var mysql = require('mysql');
var connection = mysql.createConnection(dbconfig.connection); 

module.exports = function(app) {
	app.get('/', loginCheck,function(req,res){
		var row = [];
		var row2=[];
		// connection.query('select * from users where id = ?',[req.user.id], function (err, rows) {
		connection.query('select * from users', function (err, rows) {
			if (err) {
				console.log(err);
			} else {
				if (rows.length) {
					for (var i = 0, len = rows.length; i < len; i++) { 
						row[i] = rows[i];
						
					}  
				}
			}
			res.render('pages/index', {rows : row});
		});
	});

	app.get('/about', function(req, res) {
		res.render('pages/aboutus');
		// res.sendFile(__dirname + '/views/aboutus.ejs');
	});

	app.get('/contact-us', function(req, res) {
		res.render('pages/contact_us');
	});

	app.get('/home', loginCheck, function(req, res) {
		res.render('pages/home');
	});

	app.route('/login')
		.get((req, res) => {
			res.render('pages/login');
		})
		.post((req, res) => {
			var uname = req.body.uname;
			var password = req.body.password;
			connection.query("SELECT * FROM users where username='"+uname+"' and password='"+password+"'", function (err, rows) {
				if (err) {
					console.log(err);
				} else {
					if (rows.length == 1) {
						var sess=req.session;
						sess.loggedUser = req.body.uname;
						app.locals.loggedUser = req.session.loggedUser;
						res.redirect('/home');
					} else {
						res.render('pages/login', {errorMsg : 'Invalid Login Details'});
					}
				}
			});
		});

	app.route('/register')
		.get((req, res) => {
			res.render('pages/register');
			// res.sendFile(__dirname + '/public/signup.html');
		})
		.post((req, res) => {
			var act_name = req.body.act_name;
			var dob = req.body.dob;
			var ph_no = req.body.ph_no;
			var uname = req.body.uname;
			var password = req.body.password;
			var emial = req.body.email;
			connection.query('insert into users(`name`, `username`, `password`, `date_of_birth`, `phone_no`, `email`) values("'+act_name+'","'+uname+'","'+password+'","'+dob+'","'+ph_no+'","'+emial+'")', function (err, rows) {
				if (err) {
					console.log(err);
				} else {
					var sess=req.session;
					sess.successMsg = 'successfully registered';
					res.redirect('/login');
				}
			});
		});

	app.get('/logout',function(req,res){
		req.session.destroy(function(err) {
			if(err) {
				console.log(err);
			} else {
				app.locals.loggedUser = null;
				res.redirect('/');
			}
		});
	});

	app.get('/get-users',(req,res)=>{
		connection.connect(function(err) {
			var sql= "select * from users";
			connection.query(sql, function (err, result) {
				if (err) throw err;
				res.send({'status':true,'msg':"DB checking","result":result});
			});
		});
	});

	// app.use(function(req, res, next) {
	// 	app.locals.loggedUser = req.session.loggedUser;
	// 	next();
	// });
};

function loginCheck(req, res, next) {
	if (!req.session.loggedUser) {
		res.redirect('/login');
	} else {
		next();
	}
}