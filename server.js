// server.js
// load the things we need
var express = require('express');
var session = require('express-session');
var bodyparser = require('body-parser');
var mysql = require('mysql');
var config = require('./config/conf');

// set the view engine to ejs
var app = express();
app.set('view engine', 'ejs');
app.set('port',process.env.port || 3000 );

//========= initialize body parser ==========
app.use(bodyparser.urlencoded({
	extended: true
}));
app.use(bodyparser.json());

//========= connect to db ==========
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database:"int_proj_1"
});

con.connect(function(err) {
	if (err) throw err;
	console.log("Connected to DB!");
});

app.use(session({secret:'app'}));


require('./app/routes.js')(app);
app.listen(app.get('port'),()=>{
	console.log('Server started in '+app.get('port')+" port");
});